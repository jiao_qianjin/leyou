package com.leyou.search.client;

import com.leyou.item.api.CategoryApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Auther:焦前进
 * @Data:2020/2/27
 * @Description:
 */

@FeignClient(value = "item-service")
public interface CategoryClient extends CategoryApi {
}

