package com.leyou.item.mapper;

import com.leyou.item.pojo.Spu;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Auther:焦前进
 * @Data:2020/2/23
 * @Description:
 */
public interface SpuMapper extends Mapper<Spu> {
}
