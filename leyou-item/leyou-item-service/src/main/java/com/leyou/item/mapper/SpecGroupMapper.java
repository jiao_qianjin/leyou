package com.leyou.item.mapper;

import com.leyou.item.pojo.SpecGroup;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Auther:焦前进
 * @Data:2020/2/19
 * @Description:
 */
public interface SpecGroupMapper extends Mapper<SpecGroup> {
}
