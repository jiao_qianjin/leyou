package com.leyou.item.mapper;

import com.leyou.item.pojo.Sku;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Auther:焦前进
 * @Data:2020/2/23
 * @Description:
 */
public interface SkuMapper extends Mapper<Sku> {
}
