package com.leyou.item.mapper;

import com.leyou.item.pojo.Stock;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Auther:焦前进
 * @Data:2020/2/23
 * @Description:
 */
public interface StockMapper extends Mapper<Stock> {
}
